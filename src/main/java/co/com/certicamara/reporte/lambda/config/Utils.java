package co.com.certicamara.reporte.lambda.config;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Singleton;

@Singleton
public class Utils {

    @ConfigProperty(name = "URL")
    public String url;

    @ConfigProperty(name = "USER")
    public String user;

    @ConfigProperty(name = "PASSWORD")
    public String password;

}
