package co.com.certicamara.reporte.lambda.model.dto;

import java.io.Serializable;
import java.util.Objects;

public class TipoOrdenPedido implements Serializable {

    private Long ordenPedidoId;

    private Long modeloVolumenId;

    public Long getOrdenPedidoId() {
        return ordenPedidoId;
    }

    public void setOrdenPedidoId(Long ordenPedidoId) {
        this.ordenPedidoId = ordenPedidoId;
    }

    public Long getModeloVolumenId() {
        return modeloVolumenId;
    }

    public void setModeloVolumenId(Long modeloVolumenId) {
        this.modeloVolumenId = modeloVolumenId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoOrdenPedido that = (TipoOrdenPedido) o;
        return Objects.equals(ordenPedidoId, that.ordenPedidoId);
    }

}
