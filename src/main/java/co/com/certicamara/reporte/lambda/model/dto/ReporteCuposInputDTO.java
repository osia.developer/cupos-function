package co.com.certicamara.reporte.lambda.model.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class ReporteCuposInputDTO implements Serializable {

    private Date fechaInicial;

    private Date fechaFinal;

    private Collection<Long> ordenesPedidoId;

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Collection<Long> getOrdenesPedidoId() {
        return ordenesPedidoId;
    }

    public void setOrdenesPedidoId(Collection<Long> ordenesPedidoId) {
        this.ordenesPedidoId = ordenesPedidoId;
    }

    public String toQuery() {
        String commaseparatedlist = this.ordenesPedidoId.toString();
        return commaseparatedlist.replace("[", "('")
                .replace("]", "')")
                .replace(", ", "', '");
    }

    @Override
    public String toString() {
        return "ReporteCuposInputDTO{" +
                "fechaInicial=" + fechaInicial +
                ", fechaFinal=" + fechaFinal +
                ", ordenesPedidoId=" + ordenesPedidoId +
                '}';
    }

}
