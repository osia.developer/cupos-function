package co.com.certicamara.reporte.lambda.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ReporteCupoDTO implements Serializable {

    private Long ordenPedidoId;

    private Long solicitudTitularId;

    private String nombreCompleto;

    private Long numeroIdentificacion;

    private Long numeroIdentificacionCliente;

    private String proyecto;

    private String tipoCertificado;

    private LocalDateTime fechaEmision;

    private String numeroContrato;

    private String razonSocial;

    private Long nitContrato;

    private String nombreProyecto;

    private Integer cantidadAsignada;

    private Integer cantidadDisponible;

    private LocalDateTime fechaPlazoInicial;

    private LocalDateTime fechaPlazoFinal;

    private Long pendientePorRegistrar;

    public Long getOrdenPedidoId() {
        return ordenPedidoId;
    }

    public void setOrdenPedidoId(Long ordenPedidoId) {
        this.ordenPedidoId = ordenPedidoId;
    }

    public Long getSolicitudTitularId() {
        return solicitudTitularId;
    }

    public void setSolicitudTitularId(Long solicitudTitularId) {
        this.solicitudTitularId = solicitudTitularId;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Long getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(Long numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public Long getNumeroIdentificacionCliente() {
        return numeroIdentificacionCliente;
    }

    public void setNumeroIdentificacionCliente(Long numeroIdentificacionCliente) {
        this.numeroIdentificacionCliente = numeroIdentificacionCliente;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getTipoCertificado() {
        return tipoCertificado;
    }

    public void setTipoCertificado(String tipoCertificado) {
        this.tipoCertificado = tipoCertificado;
    }

    public LocalDateTime getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(LocalDateTime fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Long getNitContrato() {
        return nitContrato;
    }

    public void setNitContrato(Long nitContrato) {
        this.nitContrato = nitContrato;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getCantidadAsignada() {
        return cantidadAsignada;
    }

    public void setCantidadAsignada(Integer cantidadAsignada) {
        this.cantidadAsignada = cantidadAsignada;
    }

    public Integer getCantidadDisponible() {
        return cantidadDisponible;
    }

    public void setCantidadDisponible(Integer cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }

    public LocalDateTime getFechaPlazoInicial() {
        return fechaPlazoInicial;
    }

    public void setFechaPlazoInicial(LocalDateTime fechaPlazoInicial) {
        this.fechaPlazoInicial = fechaPlazoInicial;
    }

    public LocalDateTime getFechaPlazoFinal() {
        return fechaPlazoFinal;
    }

    public void setFechaPlazoFinal(LocalDateTime fechaPlazoFinal) {
        this.fechaPlazoFinal = fechaPlazoFinal;
    }

    public Long getPendientePorRegistrar() {
        return pendientePorRegistrar;
    }

    public void setPendientePorRegistrar(Long pendientePorRegistrar) {
        this.pendientePorRegistrar = pendientePorRegistrar;
    }

}
