package co.com.certicamara.reporte.lambda.handler;

import co.com.certicamara.reporte.lambda.model.dto.ReporteCupoDTO;
import co.com.certicamara.reporte.lambda.model.dto.ReporteCuposInputDTO;
import co.com.certicamara.reporte.lambda.service.ReporteCuposService;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

@Named("cupos")
public class ReporteCuposLambda implements RequestHandler<ReporteCuposInputDTO, Collection<ReporteCupoDTO>> {

    @Inject
    ReporteCuposService service;

    @Override
    public Collection<ReporteCupoDTO> handleRequest(ReporteCuposInputDTO input, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("Inicio lambda ListarProductosLambda " + input.toString());
        try {
            return this.service.generarReporte(input);
        } catch (Exception e) {
            logger.log("Error lambda ListarProductosLambda: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
