package co.com.certicamara.reporte.lambda.service;

import co.com.certicamara.reporte.lambda.config.Query;
import co.com.certicamara.reporte.lambda.config.Utils;
import co.com.certicamara.reporte.lambda.model.dto.ReporteCupoDTO;
import co.com.certicamara.reporte.lambda.model.dto.ReporteCuposInputDTO;
import co.com.certicamara.reporte.lambda.model.dto.TipoOrdenPedido;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ReporteCuposService {

    @Inject
    Utils utils;

    @Inject
    Query query;

    public Collection<ReporteCupoDTO> generarReporte(ReporteCuposInputDTO inputDTO) throws Exception {
        if (inputDTO == null || (inputDTO.getOrdenesPedidoId() == null || inputDTO.getOrdenesPedidoId().isEmpty())) {
            return this.consultar();
        }
        Collection<TipoOrdenPedido> tipos = this.obtenerTiposOP(inputDTO);
        Collection<Long> tipo1 = tipos.stream().filter(tipo -> tipo.getModeloVolumenId().equals(1L))
                .map(TipoOrdenPedido::getOrdenPedidoId).collect(Collectors.toList());
        Collection<Long> tipo2 = tipos.stream().filter(tipo -> tipo.getModeloVolumenId().equals(2L))
                .map(TipoOrdenPedido::getOrdenPedidoId).collect(Collectors.toList());
        return this.consultar(tipo1, tipo2);
    }

    public Collection<ReporteCupoDTO> consultar(Collection<Long> tipo1, Collection<Long> tipo2) throws Exception {
        String sql = this.query.tipo1Query +
                this.listToQuery(tipo1) +
                " UNION ALL " +
                this.query.tipo2Query +
                this.listToQuery(tipo2) +
                this.query.grupoTipo2Query;
        try (Connection connection =
                     DriverManager.getConnection(this.utils.url, this.utils.user, this.utils.password);
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            ResultSet resultSet = pstmt.executeQuery();
            List<ReporteCupoDTO> productos = new ArrayList<>();
            while (resultSet.next()) {
                ReporteCupoDTO dto = new ReporteCupoDTO();
                dto.setOrdenPedidoId(resultSet.getLong("ORDEN_PEDIDO_ID"));
                dto.setSolicitudTitularId(resultSet.getLong("SOLICITUD_TITULAR_ID"));
                dto.setNombreCompleto(resultSet.getString("NOMBRE_COMPLETO"));
                dto.setNumeroIdentificacion(resultSet.getLong("NUMERO_IDENTIFICACION_TITULAR"));
                dto.setRazonSocial(resultSet.getString("EMPRESA"));
                dto.setNumeroIdentificacionCliente(resultSet.getLong("NIT"));
                productos.add(dto);
            }
            return productos;
        } catch (SQLException se) {
            throw new Exception(se.getMessage());
        }
    }

    public Collection<ReporteCupoDTO> consultar() throws Exception {
        String sql = this.query.tipo1QueryLimit +
                " UNION ALL " +
                this.query.tipo2QueryLimit;
        try (Connection connection =
                     DriverManager.getConnection(this.utils.url, this.utils.user, this.utils.password);
             PreparedStatement pstmt = connection.prepareStatement(sql)) {
            ResultSet resultSet = pstmt.executeQuery();
            List<ReporteCupoDTO> productos = new ArrayList<>();
            while (resultSet.next()) {
                ReporteCupoDTO dto = new ReporteCupoDTO();
                dto.setOrdenPedidoId(resultSet.getLong("ORDEN_PEDIDO_ID"));
                dto.setSolicitudTitularId(resultSet.getLong("SOLICITUD_TITULAR_ID"));
                dto.setNombreCompleto(resultSet.getString("NOMBRE_COMPLETO"));
                dto.setNumeroIdentificacion(resultSet.getLong("NUMERO_IDENTIFICACION_TITULAR"));
                dto.setRazonSocial(resultSet.getString("EMPRESA"));
                dto.setNumeroIdentificacionCliente(resultSet.getLong("NIT"));
                productos.add(dto);
            }
            return productos;
        } catch (SQLException se) {
            throw new Exception(se.getMessage());
        }
    }

    private Collection<TipoOrdenPedido> obtenerTiposOP(ReporteCuposInputDTO inputDTO) throws Exception {
        try (Connection connection =
                     DriverManager.getConnection(this.utils.url, this.utils.user, this.utils.password);
             PreparedStatement pstmt = connection.prepareStatement(this.query.tipoOpQuery + inputDTO.toQuery())) {
            ResultSet resultSet = pstmt.executeQuery();
            List<TipoOrdenPedido> tipos = new ArrayList<>();
            while (resultSet.next()) {
                TipoOrdenPedido dto = new TipoOrdenPedido();
                dto.setOrdenPedidoId(resultSet.getLong("ORDEN_PEDIDO_ID"));
                dto.setModeloVolumenId(resultSet.getLong("MODELO_VOLUMEN_ID"));
                tipos.add(dto);
            }
            return tipos;
        } catch (SQLException se) {
            throw new Exception(se.getMessage());
        }
    }

    private String listToQuery(Collection<Long> list) {
        String commaseparatedlist = list.toString();
        return commaseparatedlist.replace("[", "('")
                .replace("]", "')")
                .replace(", ", "', '");
    }

}
